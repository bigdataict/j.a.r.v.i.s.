<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <title>Jarvis - Properties</title>

  <!-- Bootstrap -->
  <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" rel="stylesheet">
  <link href='https://fonts.googleapis.com/css?family=Share+Tech+Mono' rel='stylesheet' type='text/css'>
  <link href="loading.css" rel="stylesheet">

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body>
  <nav class="navbar navbar-default navbar-static-top">
    <div class="container">
      <!-- Brand and toggle get grouped for better mobile display -->
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="#" style="font-family: 'Share Tech Mono';">J.A.R.V.I.S.</a>
      </div>

      <!-- Collect the nav links, forms, and other content for toggling -->
      <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
        <ul class="nav navbar-nav">
          <li class="active"><a href="index.php">Properties</a></li>
          <li><a href="mollen.php">Mollen</a></li>
        </ul>
      </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
  </nav>
  <div id="app">
    <div class="container">
      <div class="row">
        <div class="col-md-9">
          <div v-if="currentParam && !loading">
            <a href="#" class="btn btn-default" v-on:click="downloadParam()">Download JSON</a>
            <hr>
            <div class="alert alert-info">
              Alleen de eerste 100 resultaten worden weergegeven, voor het volledige resultaat kan je het bestand downloaden.
            </div>

            <p v-if="items.length <= 0">Geen resultaten.</p>
          </div>

          <div v-if="!currentParam">
            <p>Je hebt nog geen parameter gekozen.</p>
          </div>

          <div v-if="loading" class="loading-box">
            <span class="sr-only">Loading...</span>
            <div class="sk-folding-cube">
              <div class="sk-cube1 sk-cube"></div>
              <div class="sk-cube2 sk-cube"></div>
              <div class="sk-cube4 sk-cube"></div>
              <div class="sk-cube3 sk-cube"></div>
            </div>
          </div>

          <ul v-if="!loading">
            <li v-for="item in items">
              {{ item | json }}
            </li>
          </ul>
        </div>
        <div class="col-md-3">
          <div style="overflow: auto;max-height: 700px;">
            <ul class="nav nav-pills nav-stacked">
              <li v-for="param in params" v-bind:class="currentParam == param ? 'active' : ''"><a href="#" v-on:click="getParam(param)">{{ param }}</a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  <!-- Include all compiled plugins (below), or include individual files as needed -->
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>

  <script src="vue.js"></script>
  <script src="vue-resource.js"></script>

  <script src="jarvis.js"></script>
</body>
</html>
