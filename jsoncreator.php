<?php

include 'config.inc.php';

$limit = 'LIMIT 100';

if(isset($_GET['type'])) {
  $type = $_GET['type'];
}
else {
  $type = 'page';
}

if($type == 'download') {
  $limit = '';
}
if(isset($_GET['p'])) {
  $param = $_GET['p'];
}
else {
  $param = '*';
}
$query = $con->query("SELECT " . $param . " FROM tweets " . $limit);
$row = $query->fetchAll(PDO::FETCH_ASSOC);

$data = [];

foreach ($row as $tweet)
{
  $content = [];
  foreach ($tweet as $key => $value)
  {
    $content[$key] = json_decode($value);
  }

  array_push($data, $content);
  
}

if($type == 'page') {
  header('Content-Type: application/json');
  echo json_encode($data);
}
elseif($type == 'download') {
  header('Content-Description: File Transfer');
  header('Content-Type: application/json');
  header('Content-Disposition: attachment; filename=' . $param . '.json');
  header('Content-Transfer-Encoding: binary');
  header('Expires: 0');
  header('Cache-Control: must-revalidate, post-check=0, pre-check=0');

  echo json_encode($data);
}

?>
