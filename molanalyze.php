<?php

include 'config.inc.php';

$week_range = [
  1 => ['2015-12-26', '2016-01-02'],
  2 => ['2016-01-02', '2016-01-09'],
  3 => ['2016-01-09', '2016-01-16'],
  4 => ['2016-01-16', '2016-01-23'],
  5 => ['2016-01-23', '2016-01-30'],
  6 => ['2016-01-30', '2016-02-06'],
  7 => ['2016-02-06', '2016-02-13'],
  8 => ['2016-02-13', '2016-02-20'],
  9 => ['2016-02-20', '2016-02-27'],
  10 => ['2016-02-27', '2016-03-05'],
  11 => ['2016-02-05', '2016-03-25'],
  'overzicht' => ['2016-01-02', '2016-03-25']
];

if(isset($_GET['w'])) {
  $week = $_GET['w'];
  $week1 = $week_range[$week][0];
  $week2 = $week_range[$week][1];
}
else {
  $week1 = $week_range['overzicht'][0];
  $week2 = $week_range['overzicht'][1];
}

$string = '';


$query = $con->query("SELECT text FROM tweets WHERE php_date>='$week1' AND php_date< '$week2'");
$row = $query->fetchAll(PDO::FETCH_ASSOC);

foreach ($row as $text)
{
  $string .= json_decode($text['text']) . " ";
}

$string = strtolower($string);

$airen_count = substr_count($string, 'airen');
$annemieke_count = substr_count($string, 'annemieke');
$cecile_count = substr_count($string, 'cécile') + substr_count($string, 'cecile');
$ellie_count = substr_count($string, 'ellie');
$klaas_count = substr_count($string, 'klaas');
$marjolein_count = substr_count($string, 'marjolein');
$remy_count = substr_count($string, 'remy');
$rop_count = substr_count($string, 'rop');
$taeke_count = substr_count($string, 'taeke') + substr_count($string, 'teake');
$tim_count = substr_count($string, 'tim');

$data = [];

$data[0] = ['naam' => 'Airen', 'count' => $airen_count, 'mol' => false];
$data[1] = ['naam' => 'Annemieke', 'count' => $annemieke_count, 'mol' => false];
$data[2] = ['naam' => 'Cécile', 'count' => $cecile_count, 'mol' => false];
$data[3] = ['naam' => 'Ellie', 'count' => $ellie_count, 'mol' => false];
$data[4] = ['naam' => 'Klaas', 'count' => $klaas_count, 'mol' => false];
$data[5] = ['naam' => 'Marjolein', 'count' => $marjolein_count, 'mol' => false];
$data[6] = ['naam' => 'Remy', 'count' => $remy_count, 'mol' => false];
$data[7] = ['naam' => 'Rop', 'count' => $rop_count, 'mol' => false];
$data[8] = ['naam' => 'Taeke', 'count' => $taeke_count, 'mol' => false];
$data[9] = ['naam' => 'Tim', 'count' => $tim_count, 'mol' => false];


  header('Content-Type: application/json');
  echo json_encode($data);

?>
