var vm = new Vue({
  el: '#app',
  data: {
    loading: false,
    currentParam: '',
    items: [],
    params: [
      'created_at',
      'id_str',
      'text',
      'coordinates',
      'current_user_retweet',
      'entities',
      'favorite_count',
      'favorited',
      'in_reply_to_screen_name',
      'filter_level',
      'in_reply_to_status_id_str',
      'lang',
      'place',
      'possibly_sensitive',
      'quoted_status_id_str',
      'quoted_status',
      'scopes',
      'retweet_count',
      'retweeted',
      'retweeted_status',
      'source',
      'truncated',
      'user',
      'withheld_copyright',
      'withheld_in_countries',
      'withheld_scope'
    ]
  },
  methods: {
    getParam: function (parameter) {
      if(this.currentParam !== parameter) {
        this.currentParam = parameter;
        this.loading = true;
        this.$http({url: '/jsoncreator.php?p=' + parameter, method: 'GET'}).then(function (response) {
          console.log(response);
          this.items = response.data;
          this.loading = false;
        }, function (response) {
          this.loading = false;
        });
      }
    },
    downloadParam: function () {
      if(this.currentParam) {
        var win = window.open('/jsoncreator.php?p=' + this.currentParam + '&type=download', '_blank');
        win.focus();
      }
    }
  }
})
