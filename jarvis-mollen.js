var vm = new Vue({
  el: '#app',
  data: {
    loading: false,
    currentWeek: '',
    items: [],
    percentage: [],
    weeks: [
      1,
      2,
      3,
      4,
      5,
      6,
      7,
      8,
      9,
      10,
      11
    ]
  },
  methods: {
    getWeek: function (week) {
      if(this.currentWeek !== week) {
        this.currentWeek = week;
        this.loading = true;
        this.$http({url: '/molanalyze.php?w=' + week, method: 'GET'}).then(function (response) {
          console.log(response);
          this.items = response.data;
          this.loading = false;
        }, function (response) {
          this.loading = false;
        });
      }
    }
  }
})
